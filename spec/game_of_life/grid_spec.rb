require 'spec_helper'

module GameOfLife
  describe Grid do
    context "Next generation" do
      ALIVE = Grid::ALIVE
      DEAD = Grid::DEAD

      it "foo" do
        initial_grid = [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
        grid = Grid.new(initial_grid)
        new_grid = [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
        expect(grid.next_generation).to eq(new_grid)
      end

      it "bar" do
        initial_grid = [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
        grid = Grid.new(initial_grid)
        new_grid = [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
        expect(grid.next_generation).to eq(new_grid)        
      end
    end
  end
end
