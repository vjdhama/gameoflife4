#Conway's Game of Life

###Description

The "game" is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves or, for advanced players, by creating patterns with particular properties.

###SETUP

1. git clone https://github.com/vjdhama/gameoflife4.git
2. cd gameoflife4
3. bundle install

###BUILD

1. bundle exec rake

###Execute a spec

1. bundle exec rspec [SPECFILE_PATH:LINE]
