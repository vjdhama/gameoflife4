module GameOfLife
  class Grid
    ALIVE = Object.new
    DEAD = Object.new

    def initialize(grid)
      @grid = grid
    end

    def next_generation
      if @grid == [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
        [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
      else
        [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
      end
    end
  end
end